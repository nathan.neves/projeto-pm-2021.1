package main.java.validacartao;
import kong.unirest.*;


public class CartaoDeCredito {
	private String id;
	private String nomeTitular;
	private Long numero;
	private String validade;
	private String cvv;
	
	public  CartaoDeCredito() {
		
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getNomeTitular() {
		return this.nomeTitular;
	}
	
	public String getNumero() {
		return this.numero.toString();
	}
	
	public String getValidade() {
		return this.validade;
	}
	
	public String getCvv() {
		return this.cvv;
	}
	
	
	public CartaoDeCredito(String id, String nomeTitular, String cvv,String validade, Long numero) {
		this.id = id;
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
		this.cvv = cvv;
	}
	
	public static CartaoDeCredito hidrate(String idCiclista) {
		//valor mockado da api
		return Unirest.get("https://run.mocky.io/v3/"+idCiclista).header("Content-Type", "application/json").asObject(CartaoForm.class).getBody().hidrate();
	}
	

}
