package main.java.validacartao;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.javalin.http.Context;
import main.java.cobranca.Cobranca;
import main.java.cobranca.CobrancaSender;
import main.java.helpers.Customer;
import main.java.helpers.IncompatibleKeysException;
import main.java.json.*;
public class CartaoController {
	private CartaoController(){
	}
	
	public static void validarCartao(Context ctx) throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
		CartaoDeCredito cartao = ctx.bodyAsClass(CartaoForm.class).hidrate();
		Customer customer = new Customer("random","Guest");
		Cobranca cobranca = new Cobranca(UUID.randomUUID().toString(),cartao,customer);
		cobranca.setPrice(1);
		CobrancaSender cobrancasender = new CobrancaSender(cobranca);
		cobrancasender.montarCarrinho();
		boolean wasSent = cobrancasender.send();
		int statusCode;
		Json<String> json = new Json<>();
		
		if(cobranca.getStatus().equals("05") || !wasSent) {
			statusCode = 422;
			ctx.status(statusCode);
			json.addField("id",UUID.randomUUID().toString());
			json.addField("codigo", "InvalidCreditCardException");
			json.addField("mensagem","Cartão de crédito inválido");
			ctx.json(json.response());
			return;
		}
		
		statusCode = 200;
		json.addField("mensagem", "Cartão é valido");
		ctx.status(statusCode);
		ctx.json(json.response());
		
	}
	
}
