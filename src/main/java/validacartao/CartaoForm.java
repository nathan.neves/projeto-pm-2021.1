package main.java.validacartao;

public class CartaoForm {
	public String id;
	public String nomeTitular;
	public String numero;
	public String validade;
	public String cvv;
	
	public CartaoDeCredito hidrate() {
		return new  CartaoDeCredito(this.id,this.nomeTitular,this.cvv,this.validade,Long.parseLong(this.numero.replaceAll("[-+.^:,]","")));
	}
}
