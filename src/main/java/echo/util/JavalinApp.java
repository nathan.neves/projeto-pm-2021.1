package main.java.echo.util;

import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;
import main.java.echo.*;
import main.java.email.EmailController;
import main.java.cobranca.*;
import main.java.validacartao.*;
public class JavalinApp {
    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                    path("/:echo", () -> get(Controller::getEcho));
                    path("/", ()-> get(Controller::getRoot));
                    path("/enviarEmail",()->post(EmailController::sendEmail));
                    path("/realizarCobranca",()->post(CobrancaController::realizarCobranca));
                    path("/cobranca/:id",()->get(CobrancaController::getCobranca));
                    path("/filaCobranca",()->post(FilaCobrancaController::filaCobranca));
                    path("/validaCartaoDeCredito", ()->post(CartaoController::validarCartao));
                    });
                    


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
