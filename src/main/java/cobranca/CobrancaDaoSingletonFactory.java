package main.java.cobranca;
public class CobrancaDaoSingletonFactory {
    private static CobrancaDAO dao = null;
    
    private CobrancaDaoSingletonFactory() {}
    public static CobrancaDAO getInstance()
    {
        if (CobrancaDaoSingletonFactory.dao == null) {
            CobrancaDaoSingletonFactory.dao = new CobrancaDAO();
        }

        return CobrancaDaoSingletonFactory.dao;
    }

}
