package main.java.cobranca;

import main.java.validacartao.*;
import main.java.helpers.*;
public class Cobranca {
	private String id;
	private String MerchantOrderId;
	private Payment Payment;
	@SuppressWarnings("unused")
	private  final boolean isCryptoCurrencyNegotiation = false;
	private InternalCustomer Customer;
	private String status;
	private String horaSolicitacao;
	private String horaFinalizacao;
	private String idCiclista;
	
	public Cobranca(String OrderNumber,CartaoDeCredito cartao,Customer customer) {
		this.MerchantOrderId = OrderNumber;
		this.Payment = new Payment();
		this.Payment.CreditCard = new CreditCard();
		this.Payment.CreditCard.parse(cartao);
		this.Customer = new InternalCustomer();
		this.Customer.setName(customer.nome);
		this.Customer.setEmail(customer.email);
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setMerchantOrderId(String merchantOrderId) {
		this.MerchantOrderId = merchantOrderId;
	}
	
	public String getMerchantOrderId() {
		return this.MerchantOrderId;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setIdCiclista(String idCiclista) {
		this.idCiclista = idCiclista;
	}
	
	public String getIdCiclista() {
		return this.idCiclista;
	}
	
	public void setHoraSolicitacao(String horaInicio) {
		this.horaSolicitacao = horaInicio;
	}
	
	public void setHoraFinalizacao(String Fim) {
		this.horaFinalizacao = Fim;
	}
	
	public String getHoraFinalizacao() {
		return this.horaFinalizacao;
	} 
	

	public String getHoraSolicitacao() {
		return this.horaSolicitacao;
	} 
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	
	public boolean setPrice(int price) {
		if(price <= 0) {
			return false;
		}
		this.Payment.Amount = price;
		return true;
	}
	
	

	private class InternalCustomer{
		private  String Name;
		private String Email;
		@SuppressWarnings("unused")
		public String getName() {
			return Name;
		}
		public void setName(String name) {
			Name = name;
		}
		@SuppressWarnings("unused")
		public String getEmail() {
			return Email;
		}
		public void setEmail(String email) {
			Email = email;
		}
	}
	private class CreditCard{
		@SuppressWarnings("unused")
		public String CardNumber;
		@SuppressWarnings("unused")
		public String Holder;
		@SuppressWarnings("unused")
		public String ExpirationDate;
		@SuppressWarnings("unused")
		public String SecurityCode;
		
		public void parse(CartaoDeCredito cartao) {
			this.CardNumber = cartao.getNumero();
			this.ExpirationDate = cartao.getValidade();
			this.Holder = cartao.getNomeTitular();
			this.SecurityCode = cartao.getCvv();
		}
	}
	private class Payment{
		public CreditCard CreditCard;
		@SuppressWarnings("unused")
		public final String Type = "CreditCard";
		@SuppressWarnings("unused")
		public int Amount = 0;
		@SuppressWarnings("unused")
		public final int  Installments = 1;
		
	}
	
}
