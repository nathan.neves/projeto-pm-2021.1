package main.java.cobranca;
import java.util.LinkedList;
import java.util.Queue;

public class FilaCobrancaDAO extends CobrancaDAO {
	private Queue<Cobranca> cobrancas;
	
	
	public FilaCobrancaDAO() {
		this.setCobrancas(new LinkedList<>());
	}

	public Queue<Cobranca> getCobrancas() {
		return cobrancas;
	}

	public void setCobrancas(Queue<Cobranca> cobrancas) {
		this.cobrancas = cobrancas;
	}
	
	
}
