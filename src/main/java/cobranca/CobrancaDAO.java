package main.java.cobranca;
import java.util.ArrayList;


public class CobrancaDAO {
	private ArrayList<Cobranca> cobrancas;
    
	public CobrancaDAO() {
		this.cobrancas = new ArrayList<>();
	}
	
	
	public Cobranca get(String id){
		for(Cobranca cobranca : this.cobrancas) {
			if(cobranca.getId().equals(id)) {
				return cobranca;
			}
		}
		return null;
	}
	
	
	public void add(Cobranca cobranca) {
		Cobranca cobrancaExistente = this.get(cobranca.getId());
		if(cobrancaExistente != null) {
			this.cobrancas.set(this.cobrancas.indexOf(cobrancaExistente),cobranca);
			return;
		}
		this.cobrancas.add(cobranca);
	
	}
	
	public boolean delete(Cobranca cobranca) {
		Cobranca cobrancaDelete = this.get(cobranca.getId());
		if(cobrancaDelete != null) {
			this.cobrancas.remove(this.cobrancas.indexOf(cobrancaDelete));
			return true;
		}
		return false;
	}
	
	public String elements() {
		return this.cobrancas.toString();
	}
    
    
    
	    
}
