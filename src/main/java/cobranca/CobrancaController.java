package main.java.cobranca;
import io.javalin.http.Context;
import main.java.helpers.Customer;
import main.java.helpers.IncompatibleKeysException;
import main.java.json.Json;
import main.java.validacartao.CartaoDeCredito;
import java.sql.Timestamp;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
public class CobrancaController {
	
	protected CobrancaController() {}
	protected static  String InvalidDataException = "InvalidDataException";
	protected static  String codigo = "codigo";
	protected static  String mensagem = "mensagem";
	public static void realizarCobranca(Context ctx) throws JsonMappingException, JsonProcessingException, IncompatibleKeysException{
		Timestamp dtInicio = new Timestamp(System.currentTimeMillis());
		CobrancaForm cobrancaform = ctx.bodyAsClass(CobrancaForm.class);
		Customer customer = Customer.hidrate();
		CartaoDeCredito cartao = CartaoDeCredito.hidrate(cobrancaform.ciclista);
		Cobranca cobranca = new Cobranca(UUID.randomUUID().toString(),cartao,customer);
		boolean isValorValido = cobranca.setPrice(cobrancaform.valor);
		Json<String> json = new Json<>();
		if(!isValorValido) { 
			json.addField("id",UUID.randomUUID().toString());
			json.addField(codigo, InvalidDataException);
			json.addField(mensagem, "Preco Inválido");
			ctx.status(422);
			ctx.json(json.response());
			return;
		}
		CobrancaSender cobrancaSender = new CobrancaSender(cobranca);
		cobrancaSender.montarCarrinho();
		boolean wasSent = cobrancaSender.send();
		if(cobranca.getStatus().equals("05") || !wasSent) {
			ctx.status(422);
			json.addField("id",UUID.randomUUID().toString());
			json.addField(codigo, "InvalidStatusException");
			json.addField(mensagem,"Cartão de crédito inválido");
			ctx.json(json.response());
			return;
		}
		Timestamp dtFim = new Timestamp(System.currentTimeMillis());
		ctx.status(200);
		String strDtInicio = dtInicio.toString();
		String strDtFim = dtFim.toString();
		cobranca.setHoraSolicitacao(strDtInicio);
		cobranca.setHoraFinalizacao(strDtFim);
		cobranca.setIdCiclista(customer.id);
		json.addField("id",cobranca.getId());
		json.addField("status",cobranca.getStatus());
		json.addField("horaSolicitacao",strDtInicio);
		json.addField("horaFinalizacao",strDtFim);
		json.addField("valor",Integer.toString(cobrancaform.valor));
		json.addField("ciclista", customer.id);
		CobrancaDaoSingletonFactory.getInstance().add(cobranca);
		ctx.json(json.response());
		return;
	}
	
	public static void getCobranca(Context ctx) throws JsonMappingException, JsonProcessingException, IncompatibleKeysException{
		String idCobranca = ctx.pathParam("id");
		Cobranca cobranca = CobrancaDaoSingletonFactory.getInstance().get(idCobranca);
		Json<Cobranca> json = new Json<>();
		int statusCode;
		if(cobranca == null) {
			statusCode = 500;
			ctx.status(statusCode);
			json.addField("id",UUID.randomUUID().toString());
			json.addField(codigo, InvalidDataException);
			json.addField(mensagem, "Cobranca não encontrada");
			ctx.json(json.response());
			return;
		}
		statusCode = 200;
		ctx.status(statusCode);
		ctx.json(cobranca);
	}
	
	
	
	
}
