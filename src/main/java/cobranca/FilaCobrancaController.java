package main.java.cobranca;

import java.sql.Timestamp;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.javalin.http.Context;
import main.java.helpers.Customer;
import main.java.helpers.IncompatibleKeysException;
import main.java.json.Json;
import main.java.validacartao.CartaoDeCredito;

public class FilaCobrancaController extends CobrancaController {
	private FilaCobrancaController(){
		super();
	}
	
	//Rota de fila cobranca
	public static void filaCobranca(Context ctx) throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
		Timestamp tsDtInicio = new Timestamp(System.currentTimeMillis());
		CobrancaForm formCobranca = ctx.bodyAsClass(CobrancaForm.class);
		Customer customer = Customer.hidrate();
		CartaoDeCredito cartao = CartaoDeCredito.hidrate(formCobranca.ciclista);
		Cobranca cobranca = new Cobranca(UUID.randomUUID().toString(),cartao,customer);
		boolean isValorValido = cobranca.setPrice(formCobranca.valor);
		int statusCode;
		Json<String> json = new Json<>();
		if(!isValorValido) {
			statusCode = 422; 
			json.addField("id",UUID.randomUUID().toString());
			json.addField(codigo, InvalidDataException);
			json.addField(mensagem, "Preco Inválido");
			ctx.status(statusCode);
			ctx.json(json.response());
			return;
		}
		CobrancaSender cobrancaSender = new CobrancaSender(cobranca);
		cobrancaSender.montarCarrinho();
		boolean wasSent = cobrancaSender.send();
		if(cobranca.getStatus().equals("05") || !wasSent) {
			statusCode = 500;
			ctx.status(statusCode);
			json.addField("id",UUID.randomUUID().toString());
			json.addField(codigo, "InvalidStatusException");
			json.addField(mensagem,"Cartão de crédito inválido");
			ctx.json(json.response());
			return;
		}
		statusCode = 200;
		Timestamp dtFim = new Timestamp(System.currentTimeMillis());
		ctx.status(statusCode);
		String strDtInicio = tsDtInicio.toString();
		String strDtFim = dtFim.toString();
		cobranca.setHoraSolicitacao(strDtInicio);
		cobranca.setHoraFinalizacao(strDtFim);
		cobranca.setIdCiclista(customer.id);
		json.addField("id",cobranca.getId());
		json.addField("status",cobranca.getStatus());
		json.addField("horaSolicitacao",strDtInicio);
		json.addField("horaFinalizacao",strDtFim);
		json.addField("valor",Integer.toString(formCobranca.valor));
		json.addField("ciclista", customer.id);
		FilaCobrancaDaoSingletonFactory.getInstance().add(cobranca);
		CobrancaDaoSingletonFactory.getInstance().add(cobranca);
		ctx.json(json.response());
		return;
	}
}
