package main.java.cobranca;

public class FilaCobrancaDaoSingletonFactory {
	 private static FilaCobrancaDAO dao = null;
	    
	 	private FilaCobrancaDaoSingletonFactory() {}
	 
	    public static CobrancaDAO getInstance()
	    {
	        if (FilaCobrancaDaoSingletonFactory.dao == null) {
	            FilaCobrancaDaoSingletonFactory.dao = new FilaCobrancaDAO();
	        }

	        return FilaCobrancaDaoSingletonFactory.dao;
	    }

}
