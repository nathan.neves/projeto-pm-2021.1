package main.java.cobranca;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

public class CobrancaSender {
	private String idClient;
	private final String merchantId = "591771ab-28d1-4409-8ddc-5d516a3699bb";
	private final String url = "https://apisandbox.cieloecommerce.cielo.com.br/1/sales";
	private final String merchantKey = "NBZLSPFHSWSKWAGQWDHQOUXRRTADWGRTLUTCOUSV";
	private boolean isMontado = false; 
	private Cobranca cobranca;
	public CobrancaSender(Cobranca cobranca) {
		this.cobranca = cobranca;
	}
	
	
	public void montarCarrinho(){
		this.isMontado = true;
	}
	
	public boolean send() {
		if(!this.isMontado) {
			return false;
		}
		HttpResponse<JsonNode> response = Unirest.post(this.url)
				.header("MerchantId", this.merchantId)
				.header("MerchantKey",this.merchantKey)
			    .header("Content-Type", "application/json")
				.body(this.cobranca).asJson();
		int status = response.getStatus();
		if(status == 201 || status == 200 ) {
			JSONObject obj = response.getBody().getObject();
			cobranca.setId(obj.getJSONObject("Payment").getString("PaymentId"));
			cobranca.setStatus(obj.getJSONObject("Payment").getString("ReturnCode"));
			return true;
		}
		return false;
	}


	public String getIdClient() {
		return idClient;
	}


	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
	
	

	
}
