package main.java.helpers;
import main.java.validacartao.*;
import kong.unirest.*;
public class Customer {
	public String id;
	public String nome;
	public String status;
	public String nascimento;
	public Passaporte passaporte;
	public String nacionalidade;
	public String email;
	
	
	public static Customer hidrate() {
		
		//valor mockado da api
		return  Unirest.get("https://run.mocky.io/v3/5faf8bee-517c-4469-b39c-a636d4e1c3f2").header("Content-Type", "application/json").asObject(Customer.class).getBody();

	}
	
	public Customer(String id,String nome) {
		this.id = id;
		this.nome = nome;
	}
	
	
	private class Passaporte{
		@SuppressWarnings("unused")
		public  String Numero;
		@SuppressWarnings("unused")
		public String Validade;
		@SuppressWarnings("unused")
		public String Pais;
	}
}
