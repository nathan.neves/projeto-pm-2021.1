package main.java.email;
import io.javalin.http.Context;

import main.java.json.*;
import main.java.helpers.IncompatibleKeysException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

  
public class EmailController {
	
	private EmailController(){}
	
    public static void sendEmail(Context ctx) throws JsonMappingException, JsonProcessingException, IncompatibleKeysException{
        EmailForm emailForm = ctx.bodyAsClass(EmailForm.class);
        Email email = emailForm.hidrate();
        int statusCode; //status
        boolean result = EmailSender.enviarEmail(email);
        Json<String> json = new Json<>();
        if(result) {
        	json.addField("mensagem","E-mail enviado com sucesso");
        	statusCode = 200;
        	ctx.status(statusCode);
        	ctx.json(json.response());
        	return;
        }
        json.addField("mensagem","Erro ao enviar e-mail");
        statusCode = 400;
        ctx.status(statusCode);
        ctx.json(json.response());
        
        
    } 
}
