package main.java.email;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {
    private int id;
    private String address;
    private String mensagem;    
    
    public Email(String email, String mensagem){
        this.address = email;
        this.mensagem = mensagem;
    }
    

    public boolean validateEmail(){
        Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(this.address);
        return matcher.find();

    }

    public boolean validateMessage(){
        return !this.mensagem.isEmpty();
    }

    public void setEmail(String email){
        this.address = email;
    }

    public String getEmail(){
        return this.address;
    }

    public void setmensagem(String mensagem){
        this.mensagem = mensagem;
    }

    public String getMensagem(){
        return this.mensagem;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }


}
