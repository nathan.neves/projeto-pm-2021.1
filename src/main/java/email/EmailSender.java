package main.java.email;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class EmailSender {
	
	
	private EmailSender(){}
	public static boolean enviarEmail(Email email) {
		 boolean isEmailValid = email.validateEmail();
	     boolean isMessageValid = email.validateMessage();
	     if(!(isEmailValid && isMessageValid)) {
	    	 return false;
	     }
		Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.ssl.checkserveridentity", "true");
	    props.put("mail.smtp.socketFactory.class",
	    "javax.net.ssl.SSLSocketFactory");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.port", "465");

	    Session session = Session.getDefaultInstance(props,
	      new javax.mail.Authenticator() {
	    	   @Override
	           protected PasswordAuthentication getPasswordAuthentication()
	           {
	                 return new PasswordAuthentication("emailsenderunibike@gmail.com",
	                 "unibike2021");
	           }
	      });

	    session.setDebug(true);

	    try {

	      Message message = new MimeMessage(session);
	      message.setFrom(new InternetAddress("emailsenderunibike@gmail.com"));
	      //Remetente

	      Address[] toUser = InternetAddress
	                 .parse(email.getEmail());

	      message.setRecipients(Message.RecipientType.TO, toUser);
	      message.setSubject("Teste");
	      message.setText(email.getMensagem());
	      Transport.send(message);

	      return true;

	     } catch (MessagingException e) {
	        return false;
	    }
	  }
	}
	
