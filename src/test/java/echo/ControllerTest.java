package test.java.echo;

import static org.junit.jupiter.api.Assertions.*;

import java.io.Console;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import main.java.echo.util.JavalinApp;
import main.java.helpers.IncompatibleKeysException;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import main.java.json.*;



class ControllerTest {

    private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have
    public static final int defaultPort = 7000;
    public static final String baseUrl = "http://localhost:7000/";
    @BeforeAll
    static void init() {
        app.start(7000);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }
    
    @Test
    void enviarEmailTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
    	 Json<String> json = new Json<>();
    	 json.addField("email","nathann-s@outlook.com");
    	 json.addField("mensagem","Realizando um teste e nada mais"); 
    	 System.out.println(baseUrl);
    	 System.out.println("enviarEmail:"+json.response());
    	 HttpResponse<JsonNode> response = Unirest.post(baseUrl+"enviarEmail").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(200, response.getStatus());
    }
    
    @Test
    void  filaCobrancaTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
    	 Json<String> json = new Json<>();
    	 json.addField("valor","1000");
    	 json.addField("ciclista","25c03254-18a9-42e7-9b65-04cf2a8cec2c");		
    	 HttpResponse<JsonNode> response = Unirest.post(baseUrl+"filaCobranca").body(json.response().toString()).header("Content-Type", "application/json").asJson();
    	 assertEquals(200, response.getStatus());
    }
    
    @Test
    void validaCartaoDeCreditoTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
    	Json<String> json = new Json<>();
    	 json.addField("nomeTitular","Average Joe");
    	 json.addField("numero","0000000000000001");
    	 json.addField("validade","11/2022");
    	 json.addField("cvv","562");
    	 System.out.println(json.response());
    	 HttpResponse<JsonNode> response = Unirest.post(baseUrl+"validaCartaoDeCredito").header("Content-Type", "application/json").body(json.response().toString()).asJson();
    	 assertEquals(200,response.getStatus());
    }
    
    @Test
    void cobrancaTest() throws JsonMappingException, JsonProcessingException, IncompatibleKeysException {
    	Json<String> json = new Json<>();
    	json.addField("valor","100");
   	 	json.addField("ciclista","25c03254-18a9-42e7-9b65-04cf2a8cec2c");	
    	HttpResponse<JsonNode> response = Unirest.post(baseUrl+"realizarCobranca").body(json.response().toString()).header("Content-Type", "application/json").asJson();
    	assertEquals(200,response.getStatus());
    	System.out.println(response.getBody());
    	System.out.println(response.getBody().getObject());
    	System.out.println(response.getBody().getObject().getString("id"));
    	String idCobranca = response.getBody().getObject().getString("id");
    	HttpResponse<JsonNode> responseCobranca = Unirest.get(baseUrl+"cobranca/"+idCobranca).asJson();
    	assertEquals(200,responseCobranca.getStatus());
    }
    
    
   
    
    
}
